package com.petrovym.markup.utilits;

import android.content.SharedPreferences;

import com.petrovym.markup.main.MainActivity;

public abstract class SettingsApp {

    private static SharedPreferences sharedPreferences=MainActivity.getSharedPreferences();;
    private static boolean bol_isUseNDS =false;
    private static String str_defaultSizeNDS="18";
    private static String str_defaultMarkup="";
    private static String str_defaultSalary="";
    private static String str_defaultPercent="";
    private static String str_defaultStartWindow="";
    private static String str_defaultTheme="";
    private static String str_defaultLang="";

    public static boolean isBol_isUseNDS() {
        bol_isUseNDS =sharedPreferences.getBoolean("is_use_nds", false);
        return bol_isUseNDS;
    }

    public static String getStr_defaultSizeNDS() {
        str_defaultSizeNDS=sharedPreferences.getString("default_size_nds","18");
        return str_defaultSizeNDS;
    }

    public static String getStr_defaultMarkup() {
        str_defaultMarkup=sharedPreferences.getString("default_markup","");
        return str_defaultMarkup;
    }

    public static String getStr_defaultSalary() {
        str_defaultSalary=sharedPreferences.getString("default_salary","");
        return str_defaultSalary;
    }

    public static String getStr_defaultPercent() {
        str_defaultPercent=sharedPreferences.getString("default_percent_of_salary","");
        return str_defaultPercent;
    }

    public static String getStr_defaultStartWindow() {
        str_defaultStartWindow=sharedPreferences.getString("default_start_window","0");
        return str_defaultStartWindow;
    }

    public static String getStr_defaultTheme() {
        str_defaultTheme=sharedPreferences.getString("default_theme","0");
        return str_defaultTheme;
    }

    public static String getStr_defaultLang() {
        str_defaultLang=sharedPreferences.getString("default_lang","0");
        return str_defaultLang;
    }
}
