package com.petrovym.markup.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.petrovym.markup.R;
import com.petrovym.markup.main.MainActivity;
import com.petrovym.markup.utilits.SettingsApp;

import java.util.Locale;

public class FragmentCalcPrimeCoast extends Fragment {

    //region Fields
    private EditText et_priceGoods;
    private EditText et_markup;
    private TextView tv_result;
    private TextView tv_resultWithNDS;
    private TextView tv_lbl_primeCoastWithNDS;

    private ImageView iv_btn_clearEtPriceOfGoods;
    private ImageView iv_btn_clearEtMarkup;

    private double result = 0;
    private double resultWithNDS = 0;
    private double priceOfGoods = 0;
    private double markup = 0;

    private View rootView;

    private Boolean isChoiceNDS;
    //endregion Fields

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_calc_prime_coast_f2, container, false);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        init();
    }

    private void init() {
        isChoiceNDS = MainActivity.getSharedPreferences().getBoolean("default_use_nds", false);
        et_priceGoods = (EditText) rootView.findViewById(R.id.f2_et_priceOfGoods);
        et_markup = (EditText) rootView.findViewById(R.id.f2_et_markup);
        tv_result = (TextView) rootView.findViewById(R.id.f2_tv_result);
        tv_lbl_primeCoastWithNDS = (TextView) rootView.findViewById(R.id.f2_tv_primeCoast_with_nds);
        tv_resultWithNDS = (TextView) rootView.findViewById(R.id.f2_tv_result_with_nds);
        iv_btn_clearEtPriceOfGoods = (ImageView) rootView.findViewById(R.id.f2_btn_clear_et_PriceOfGoods);
        iv_btn_clearEtPriceOfGoods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_priceGoods.setText("");
            }
        });
        iv_btn_clearEtMarkup = (ImageView) rootView.findViewById(R.id.f2_btn_clear_et_markup);
        iv_btn_clearEtMarkup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_markup.setText("");
            }
        });
        et_priceGoods.requestFocus();
        et_priceGoods.addTextChangedListener(et_PriceGoodsWatcher);
        et_markup.addTextChangedListener(et_MarkupWatcher);

        et_markup.setText(SettingsApp.getStr_defaultMarkup());

        if (SettingsApp.isBol_isUseNDS()) {
            tv_resultWithNDS.setVisibility(View.VISIBLE);
            tv_lbl_primeCoastWithNDS.setVisibility(View.VISIBLE);
        } else {
            tv_resultWithNDS.setVisibility(View.INVISIBLE);
            tv_lbl_primeCoastWithNDS.setVisibility(View.INVISIBLE);
        }

    }

    private final TextWatcher et_PriceGoodsWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private final TextWatcher et_MarkupWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private void updateResult() {
        if (et_markup.getText().toString().equals("")) {
            tv_result.setText("");
            tv_resultWithNDS.setText("");
        } else if (et_priceGoods.getText().toString().equals("")) {
            tv_result.setText("");
            tv_resultWithNDS.setText("");
        }

        try {
            markup = Double.parseDouble(et_markup.getText().toString());
        } catch (Exception e) {
            return;
        }
        try {
            priceOfGoods = Double.parseDouble(et_priceGoods.getText().toString());
        } catch (Exception e) {
            return;
        }

        calculationResult();

        if (result % 1 == 0) {
            tv_result.setText("" + (int) result);
        } else {
            tv_result.setText(String.format(Locale.US, "%.2f", result));
        }
        if (resultWithNDS % 1 == 0) {
            tv_resultWithNDS.setText("" + (int) resultWithNDS);
        } else {
            tv_resultWithNDS.setText(String.format(Locale.US, "%.2f", resultWithNDS));
        }

    }

    private void calculationResult() {
        double sizeNDS=(Double.parseDouble(SettingsApp.getStr_defaultSizeNDS())/100)+1;

        double percent = (markup / 100) + 1;
        result = priceOfGoods / percent;

        double tempPercentFromNDS = priceOfGoods / sizeNDS;
        double tempPriceOfGoodsWithNDS = priceOfGoods - tempPercentFromNDS;
        double tempResultWithNDS = priceOfGoods - tempPriceOfGoodsWithNDS;
        resultWithNDS = tempResultWithNDS / percent;

    }

}
