package com.petrovym.markup.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.petrovym.markup.R;

import java.util.Locale;

public class FragmentCalcDiscount extends Fragment {

    //region Fields
    private EditText et_priceOfGoods;
    private EditText et_discount;
    private TextView tv_discountPrice;
    private TextView tv_discountSize;
    private ImageView iv_btn_clear_etPriceOfGoods;
    private ImageView iv_btn_clear_etDiscounts;
    private View rootView;

    //endregion

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView= inflater.inflate(R.layout.fragment_calc_discount_f3, container, false);
        return rootView;
    }
    @Override
    public void onStart() {
        super.onStart();
        init();

    }

    private void init() {



        et_priceOfGoods = (EditText)rootView. findViewById(R.id.f3_et_priceOfGoods);
        et_discount = (EditText) rootView.findViewById(R.id.f3_et_discount);
        tv_discountPrice = (TextView) rootView.findViewById(R.id.f3_tv_discountPrice);
        tv_discountSize = (TextView) rootView.findViewById(R.id.f3_tv_discountSize);
        iv_btn_clear_etPriceOfGoods =(ImageView) rootView.findViewById(R.id.f3_btn_clear_et_priceOfGoods);
        iv_btn_clear_etPriceOfGoods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_priceOfGoods.setText("");
            }
        });

        iv_btn_clear_etDiscounts =(ImageView)rootView.findViewById(R.id.f3_btn_clear_et_discount);
        iv_btn_clear_etDiscounts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_discount.setText("");
            }
        });
        et_priceOfGoods.requestFocus();
        et_priceOfGoods.addTextChangedListener(et_priceOfGoodsWatcher);
        et_discount.addTextChangedListener(et_discountWatcher);


    }

    private final TextWatcher et_discountWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private final TextWatcher et_priceOfGoodsWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };


    private void updateResult() {
        if (et_discount.getText().toString().equals("")) {
            tv_discountPrice.setText("");
            tv_discountSize.setText("");
        } else if (et_priceOfGoods.getText().toString().equals("")) {
            tv_discountPrice.setText("");
            tv_discountSize.setText("");
        }

        double discount;
        try {
            discount = Double.parseDouble(et_discount.getText().toString());
        } catch (Exception e) {
            return;
        }
        double priceOfGoods;
        try {
            priceOfGoods = Double.parseDouble(et_priceOfGoods.getText().toString());
        } catch (Exception e) {
            return;
        }

        double discountSize = (priceOfGoods * discount) / 100;
        double discountPrice = priceOfGoods - discountSize;

        if (discountPrice % 1 == 0) {
            tv_discountPrice.setText("" + (int) discountPrice);
        } else {
            tv_discountPrice.setText(String.format(Locale.US, "%.2f", discountPrice));
        }

        if (discountSize % 1 == 0) {
            tv_discountSize.setText("" + (int) discountSize);
        } else {
            tv_discountSize.setText(String.format(Locale.US, "%.2f", discountSize));
        }
    }
}
