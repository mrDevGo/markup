package com.petrovym.markup.fragments;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.petrovym.markup.R;


public class FragmentSettings extends PreferenceFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        addPreferencesFromResource(R.xml.settings);
        //Устонавливаем значение из настроек в дополнительную строку
        bindPreferenceSummaryToValue(findPreference("default_markup"));
        bindPreferenceSummaryToValue(findPreference("default_salary"));
        bindPreferenceSummaryToValue(findPreference("default_percent_of_salary"));
        bindPreferenceSummaryToValue(findPreference("default_start_window"));
        bindPreferenceSummaryToValue(findPreference("default_size_nds"));
        bindPreferenceSummaryToValue(findPreference("default_theme"));
        bindPreferenceSummaryToValue(findPreference("default_lang"));
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    private  Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();
            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);
                preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);
            } else {
                preference.setSummary(stringValue);
            }
            return true;

        }

    };

    private  void bindPreferenceSummaryToValue(Preference preference) {
        // Задаем слушатель на изменение настроек
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
        // При изменении настроек устонавливаем новое значение в доп. строку
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(), ""));
    }

    @Override
    public void onResume() {
        super.onResume();
        Toast.makeText(getActivity(), R.string.toast_restart_app, Toast.LENGTH_SHORT).show();
    }
}
