package com.petrovym.markup.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.petrovym.markup.R;
import com.petrovym.markup.utilits.SettingsApp;

public class FragmentCalcSaleProceeds extends Fragment implements View.OnClickListener{

    //region Fields
    private EditText et_saleProceeds;
    private EditText et_salary;
    private EditText et_lease;
    private EditText et_tax;
    private EditText et_overall;
    private EditText et_markup;
    private TextView tv_result;

    private ImageView iv_btn_clear_et_saleProceeds;
    private ImageView iv_btn_clear_et_salary;
    private ImageView iv_btn_clear_et_lease;
    private ImageView iv_btn_clear_et_tax;
    private ImageView iv_btn_clear_et_overall;
    private ImageView iv_btn_clear_et_markup;


    private View rootView;

    //endregion Fields
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_calc_sale_proceeds_f4, container, false);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        init();
    }

    private void init() {
        et_saleProceeds = (EditText) rootView.findViewById(R.id.f4_et_saleProceeds);
        et_lease = (EditText) rootView.findViewById(R.id.f4_et_lease);
        et_tax = (EditText) rootView.findViewById(R.id.f4_et_tax);
        et_overall = (EditText) rootView.findViewById(R.id.f4_et_overall);
        et_markup = (EditText) rootView.findViewById(R.id.f4_et_markup);
        et_salary = (EditText) rootView.findViewById(R.id.f4_et_salary);
        tv_result = (TextView) rootView.findViewById(R.id.f4_tv_result);

        et_markup.setText(SettingsApp.getStr_defaultMarkup());

        iv_btn_clear_et_lease=(ImageView)rootView.findViewById(R.id.f4_btn_clear_et_lease);
        iv_btn_clear_et_lease.setOnClickListener(this);
        iv_btn_clear_et_markup=(ImageView)rootView.findViewById(R.id.f4_btn_clear_et_markup);
        iv_btn_clear_et_markup.setOnClickListener(this);
        iv_btn_clear_et_overall=(ImageView)rootView.findViewById(R.id.f4_btn_clear_et_overall);
        iv_btn_clear_et_overall.setOnClickListener(this);
        iv_btn_clear_et_salary=(ImageView)rootView.findViewById(R.id.f4_btn_clear_et_salary);
        iv_btn_clear_et_salary.setOnClickListener(this);
        iv_btn_clear_et_saleProceeds=(ImageView)rootView.findViewById(R.id.f4_btn_clear_et_saleProceeds);
        iv_btn_clear_et_saleProceeds.setOnClickListener(this);
        iv_btn_clear_et_tax=(ImageView)rootView.findViewById(R.id.f4_btn_clear_et_tax);
        iv_btn_clear_et_tax.setOnClickListener(this);



        et_saleProceeds.addTextChangedListener(et_saleProceedsWatcher);
        et_lease.addTextChangedListener(et_leaseWatcher);
        et_tax.addTextChangedListener(et_taxWatcher);
        et_overall.addTextChangedListener(et_overallWatcher);
        et_salary.addTextChangedListener(et_salaryWatcher);
        et_markup.addTextChangedListener(et_markupWatcher);
    }

    private final TextWatcher et_leaseWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private final TextWatcher et_taxWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();

        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private final TextWatcher et_overallWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private final TextWatcher et_markupWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private final TextWatcher et_salaryWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private final TextWatcher et_saleProceedsWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private void updateResult() {

        if (et_saleProceeds.getText().toString().equals("")) {
            tv_result.setText("");

        } else if (et_lease.getText().toString().equals("")) {
            tv_result.setText("");
        } else if (et_tax.getText().toString().equals("")) {
            tv_result.setText("");
        } else if (et_salary.getText().toString().equals("")) {
            tv_result.setText("");
        } else if (et_overall.getText().toString().equals("")) {
            tv_result.setText("");
        } else if (et_markup.getText().toString().equals("")) {
            tv_result.setText("");
        }

        double saleProceeds;
        try {
            saleProceeds = Double.parseDouble(et_saleProceeds.getText().toString());
        } catch (Exception e) {
            return;
        }
        double salary;
        try {
            salary = Double.parseDouble(et_salary.getText().toString());
        } catch (Exception e) {
            return;
        }
        double lease;
        try {
            lease = Double.parseDouble(et_lease.getText().toString());
        } catch (Exception e) {
            return;
        }
        double tax;
        try {
            tax = Double.parseDouble(et_tax.getText().toString());
        } catch (Exception e) {
            return;
        }
        double overall;
        try {
            overall = Double.parseDouble(et_overall.getText().toString());
        } catch (Exception e) {
            return;
        }
        double markup;
        try {
            markup = Double.parseDouble(et_markup.getText().toString());
        } catch (Exception e) {
            return;
        }


        double temp = (markup / 100) + 1;
        double percent = (saleProceeds / temp);

        double result = saleProceeds - percent - salary - lease - tax - overall;
        tv_result.setText("" + (int) result);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.f4_btn_clear_et_lease:
                et_lease.setText("");
                break;
            case R.id.f4_btn_clear_et_markup:
                et_markup.setText("");
                break;
            case R.id.f4_btn_clear_et_overall:
                et_overall.setText("");
                break;
            case R.id.f4_btn_clear_et_salary:
                et_salary.setText("");
                break;
            case R.id.f4_btn_clear_et_saleProceeds:
                et_saleProceeds.setText("");
                break;
            case R.id.f4_btn_clear_et_tax:
                et_tax.setText("");
                break;
        }
    }
}
