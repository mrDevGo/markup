package com.petrovym.markup.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.petrovym.markup.R;
import com.petrovym.markup.utilits.SettingsApp;

import java.util.Locale;

public class FragmentCalcPriceOfGoods extends Fragment {

    //region Fields
    private EditText et_primeCoast;
    private EditText et_markup;
    private TextView tv_result;
    private TextView tv_priceOfGoodsWithNDS;
    private TextView tv_resultWithNDS;

    private ImageView iv_btn_clearEtPrimeCoast;
    private ImageView iv_btn_clearEtMarkup;

    private View rootView;

    //endregion Fields

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_calc_price_of_goods_f1, container, false);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        init();

    }

    private void init() {
        et_primeCoast = (EditText) rootView.findViewById(R.id.f1_et_primeCoast);
        et_markup = (EditText)rootView.findViewById(R.id.f1_et_markup);
        tv_result = (TextView) rootView.findViewById(R.id.f1_tv_result);
        tv_priceOfGoodsWithNDS = (TextView) rootView.findViewById(R.id.f1_tv_priceOfGoods_with_nds);
        tv_resultWithNDS = (TextView) rootView.findViewById(R.id.f1_tv_result_with_nds);
        iv_btn_clearEtPrimeCoast = (ImageView) rootView.findViewById(R.id.f1_btn_clear_et_primeCoast);
        iv_btn_clearEtPrimeCoast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_primeCoast.setText("");
            }
        });
        iv_btn_clearEtMarkup = (ImageView) rootView.findViewById(R.id.f1_btn_clear_et_markup);
        iv_btn_clearEtMarkup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_markup.setText("");
            }
        });
        et_primeCoast.requestFocus();
        et_primeCoast.addTextChangedListener(et_primeCoastWatcher);
        et_markup.addTextChangedListener(et_MarkupWatcher);

        et_markup.setText(SettingsApp.getStr_defaultMarkup());

        if (SettingsApp.isBol_isUseNDS()) {
            tv_resultWithNDS.setVisibility(View.VISIBLE);
            tv_priceOfGoodsWithNDS.setVisibility(View.VISIBLE);
        } else {
            tv_resultWithNDS.setVisibility(View.INVISIBLE);
            tv_priceOfGoodsWithNDS.setVisibility(View.INVISIBLE);
        }
    }

    private final TextWatcher et_primeCoastWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private final TextWatcher et_MarkupWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private void updateResult() {

        if (et_markup.getText().toString().equals("")) {
            tv_result.setText("");
            tv_resultWithNDS.setText("");
        } else if (et_primeCoast.getText().toString().equals("")) {
            tv_result.setText("");
            tv_resultWithNDS.setText("");
        }

        double markup;
        try {
            markup = Double.parseDouble(et_markup.getText().toString());
        } catch (Exception e) {
            return;
        }
        double primeCoast;
        try {
            primeCoast = Double.parseDouble(et_primeCoast.getText().toString());
        } catch (Exception e) {
            return;
        }

        double sizeNDS=Double.parseDouble(SettingsApp.getStr_defaultSizeNDS());

        double percent = (primeCoast * markup) / 100;
        double result = primeCoast + percent;
        double tempResultWithNDS = (result * sizeNDS) / 100;
        double resultWithNDS = tempResultWithNDS + result;

        if (result % 1 == 0) {
            tv_result.setText("" + (int) result);
        } else {
            tv_result.setText(String.format(Locale.US, "%.2f", result));
        }
        if (resultWithNDS % 1 == 0) {
            tv_resultWithNDS.setText("" + (int) resultWithNDS);
        } else {
            tv_resultWithNDS.setText(String.format(Locale.US, "%.2f", resultWithNDS));
        }

    }

}
