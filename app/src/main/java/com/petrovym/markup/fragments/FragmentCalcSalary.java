package com.petrovym.markup.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.petrovym.markup.R;
import com.petrovym.markup.utilits.SettingsApp;

import java.util.Locale;

public class FragmentCalcSalary extends Fragment {

    //region Fields
    private EditText et_proceeds;
    private EditText et_salary;
    private EditText et_salaryPercent;

    private ImageView iv_btn_clearEtProceeds;
    private ImageView iv_btn_clearEtSalary;
    private ImageView iv_btn_clearEtSalaryPercent;

    private TextView tv_result;

    private double salary;
    private double proceeds;
    private double salaryPercent;
    private double result;

    private View rootView;
    //endregion Fields

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_calc_salary_f8, container, false);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        init();
    }

    private void init() {
        et_proceeds = (EditText) rootView.findViewById(R.id.f8_et_proceeds);
        et_salary = (EditText) rootView.findViewById(R.id.f8_et_salary);
        et_salaryPercent = (EditText) rootView.findViewById(R.id.f8_et_salaryPercent);
        tv_result = (TextView) rootView.findViewById(R.id.f8_tv_result);

        iv_btn_clearEtProceeds = (ImageView) rootView.findViewById(R.id.f8_btn_clear_et_proceeds);
        iv_btn_clearEtProceeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_proceeds.setText("");
            }
        });

        iv_btn_clearEtSalary = (ImageView) rootView.findViewById(R.id.f8_btn_clear_et_salary);
        iv_btn_clearEtSalary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_salary.setText("");
            }
        });

        iv_btn_clearEtSalaryPercent = (ImageView) rootView.findViewById(R.id.f8_btn_clear_et_salaryPercent);
        iv_btn_clearEtSalaryPercent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_salaryPercent.setText("");
            }
        });

        et_proceeds.addTextChangedListener(et_proceedsWatcher);
        et_salaryPercent.addTextChangedListener(et_salaryPercentWatcher);
        et_salary.addTextChangedListener(et_salaryWatcher);

        et_salary.setText(SettingsApp.getStr_defaultSalary());
        et_salaryPercent.setText(SettingsApp.getStr_defaultPercent());

    }

    private final TextWatcher et_proceedsWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private final TextWatcher et_salaryWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private final TextWatcher et_salaryPercentWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            updateResult();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void updateResult() {


        if (et_salaryPercent.getText().toString().equals("") ||
                et_salary.getText().toString().equals("") ||
                et_proceeds.getText().toString().equals("")) {
            tv_result.setText("");
        }

        try {
            salary = Double.parseDouble(et_salary.getText().toString());
            salaryPercent = Double.parseDouble(et_salaryPercent.getText().toString());
            proceeds = Double.parseDouble(et_proceeds.getText().toString());
        } catch (Exception e) {
            return;
        }
        calculationResult();

        if (result % 1 == 0) {
            tv_result.setText("" + (int) result);
        } else {
            tv_result.setText(String.format(Locale.US, "%.2f", result));
        }

    }

    private void calculationResult() {

        if (et_salaryPercent.getText().toString().equals("")) {
            et_salaryPercent.setText("0");
        }
        double tempPercent = (proceeds * salaryPercent) / 100;
        result = tempPercent + salary;

    }

}
