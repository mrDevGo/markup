package com.petrovym.markup.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.petrovym.markup.R;
import com.petrovym.markup.fragments.FragmentCalcDiscount;
import com.petrovym.markup.fragments.FragmentCalcPriceOfGoods;
import com.petrovym.markup.fragments.FragmentCalcPrimeCoast;
import com.petrovym.markup.fragments.FragmentCalcSalary;
import com.petrovym.markup.fragments.FragmentCalcSaleProceeds;
import com.petrovym.markup.fragments.FragmentHelp;
import com.petrovym.markup.fragments.FragmentSettings;
import com.petrovym.markup.utilits.SettingsApp;

import java.util.Locale;



public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //TODO Доделать светлую тему
    //TODO Доделать размер шрифта в настройках
    //TODO Доделать окно диалогов внутри тем

    private static SharedPreferences sharedPreferences;
    Configuration configuration=new Configuration();
    private long back_pressed;

    public static SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        initTheme();
        setContentView(R.layout.activity_main);
    }

    private void initLang() {

        switch (SettingsApp.getStr_defaultLang()){

            case "0":
                configuration.locale=(new Locale("ru"));
                getResources().updateConfiguration(configuration,getResources().getDisplayMetrics());
                break;
            case "1":
                configuration.locale=(new Locale("en"));
                getResources().updateConfiguration(configuration,getResources().getDisplayMetrics());
                break;
            case "2":
                configuration.locale=(new Locale("tr"));
                getResources().updateConfiguration(configuration,getResources().getDisplayMetrics());
                break;
            case "3":
                configuration.locale=(new Locale("tt"));
                getResources().updateConfiguration(configuration,getResources().getDisplayMetrics());
                break;
            default:
                Locale.setDefault(Locale.getDefault());
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        init();
        initStartWindow();
       initLang();
    }

    public  void initTheme() {
        switch (SettingsApp.getStr_defaultTheme()){
            case "0":
                setTheme(R.style.MainAppTheme);
                break;
            case "1":
                setTheme(R.style.DarkAppTheme);
                break;
            case "2":
                setTheme(R.style.LightAppTheme);
                break;
            default:
                setTheme(R.style.MainAppTheme);
        }
    }

    private void initStartWindow() {
        switch (SettingsApp.getStr_defaultStartWindow()){
            case "0":
                getFragmentManager().beginTransaction().replace(R.id.container,new FragmentCalcPriceOfGoods()).commit();
                setTitle(getString(R.string.nav_calPriceOfGoods));
                break;
            case "1":
                getFragmentManager().beginTransaction().replace(R.id.container,new FragmentCalcPrimeCoast()).commit();
                setTitle(getString(R.string.nav_calcPrimeCoast));
                break;
            case "2":
                getFragmentManager().beginTransaction().replace(R.id.container,new FragmentCalcDiscount()).commit();
                setTitle(getString(R.string.nav_calcDiscount));
                break;
            case "3":
                getFragmentManager().beginTransaction().replace(R.id.container,new FragmentCalcSalary()).commit();
                setTitle(getString(R.string.nav_calcSalary));
                break;
            case "4":
                getFragmentManager().beginTransaction().replace(R.id.container,new FragmentCalcSaleProceeds()).commit();
                setTitle(getString(R.string.nav_calcSaleProceeds));
                break;
            default:
                getFragmentManager().beginTransaction().replace(R.id.container,new FragmentCalcPriceOfGoods()).commit();
                setTitle(getString(R.string.nav_calPriceOfGoods));
                break;
        }
    }

    private void init() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            //При открытии шторки клавиатура скрывается
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                assert drawer != null;
                inputMethodManager.hideSoftInputFromWindow(drawer.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (back_pressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(getBaseContext(), R.string.do_for_exit, Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();

    }

    //Метод вызывает окно диалога с выбором выхода или нет.
    private void openQuitDialog() {
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(
                MainActivity.this);
        quitDialog.setTitle(getString(R.string.ad_exitProgram));
        quitDialog.setPositiveButton(getString(R.string.ad_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        quitDialog.setNegativeButton(getString(R.string.ad_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        quitDialog.show();
    }

    public boolean onNavigationItemSelected(MenuItem item) {


        int id = item.getItemId();

        if (id == R.id.nav_calPriceOfGoods) {
            getFragmentManager().beginTransaction().replace(R.id.container, new FragmentCalcPriceOfGoods()).commit();
            setTitle(getString(R.string.nav_calPriceOfGoods));

        } else if (id == R.id.nav_calcPrimeCoast) {
            getFragmentManager().beginTransaction().replace(R.id.container, new FragmentCalcPrimeCoast()).commit();
            setTitle(getString(R.string.nav_calcPrimeCoast));

        } else if (id == R.id.nav_calcDiscount) {
            getFragmentManager().beginTransaction().replace(R.id.container, new FragmentCalcDiscount()).commit();
            setTitle(getString(R.string.nav_calcDiscount));

        } else if (id == R.id.nav_calcSaleProceeds) {
            getFragmentManager().beginTransaction().replace(R.id.container, new FragmentCalcSaleProceeds()).commit();
            setTitle(getString(R.string.nav_calcSaleProceeds));

        } else if (id == R.id.nav_help) {
            getFragmentManager().beginTransaction().replace(R.id.container, new FragmentHelp()).commit();
            setTitle(getString(R.string.nav_help));

        } else if (id == R.id.nav_settings) {
            getFragmentManager().beginTransaction().replace(R.id.container, new FragmentSettings()).commit();
            setTitle(getString(R.string.nav_settings));

        } else if (id == R.id.nav_calcSalary) {
            getFragmentManager().beginTransaction().replace(R.id.container, new FragmentCalcSalary()).commit();
            setTitle(getString(R.string.nav_calcSalary));

        } else if (id == R.id.nav_mail) {
            sendingMail();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void sendingMail() {
        Intent intentSendMail = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "mr.developer@yandex.ru", null));
        intentSendMail.putExtra(Intent.EXTRA_SUBJECT, "Markup");
        startActivity(Intent.createChooser(intentSendMail, getString(R.string.sending_mail)));
    }


}
